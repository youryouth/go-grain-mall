package result

import (
	"github.com/gin-gonic/gin"
	"member-service/common"
	"net/http"
)

var ()

func OkError(context *gin.Context, err error) {
	context.JSON(http.StatusOK, common.Error.WithMsg(err.Error()))
}
func IdError(context *gin.Context) {
	context.JSON(http.StatusOK, common.Error.WithMsg("id错误"))
}

func NilError(context *gin.Context) {
	context.JSON(http.StatusOK, common.Error.WithMsg("不能保存空数据"))
}
