package dao

import (
	"errors"
	"gorm.io/gorm"
	"member-service/common"
	"member-service/dao/entity"
)

var (
	UmsMemberLevelDao = &umsMemberLevelDao{}
)

type umsMemberLevelDao struct {
}

func (this *umsMemberLevelDao) FindById(db *gorm.DB, id int64) (*entity.UmsMemberLevel, error) {
	model := &entity.UmsMemberLevel{}
	db.Find(model, id)
	if model.Id == 0 {
		return nil, errors.New("无此用户")
	}
	return model, nil
}

func (this *umsMemberLevelDao) FindAll(db *gorm.DB, query *common.PageQuery) ([]*entity.UmsMemberLevel, int64, error) {
	tx := db.Scopes(Page(int(query.Page), int(query.Size)))

	//把数据封装到any里
	var models []*entity.UmsMemberLevel

	//直接拼接所有条件
	model := query.Model.(*entity.UmsMemberLevel)

	//count不包含limit条件
	var count int64
	tx.Model(&model).Count(&count)

	err := tx.Where(model).Find(&models).Error
	if err != nil {
		return nil, 0, err
	}
	return models, count, nil
}

func (this *umsMemberLevelDao) DeleteById(db *gorm.DB, id int64) error {
	tx := db.Delete(&entity.UmsMemberLevel{}, id)
	if tx.Error != nil {
		return errors.New("无效删除")
	}
	return nil
}
func (this *umsMemberLevelDao) UpdateById(db *gorm.DB, id int64, model *entity.UmsMemberLevel) error {
	model.Id = id
	tx := db.Updates(model)
	if tx.Error != nil {
		return errors.New("无效更新")
	}
	return nil
}

func (this *umsMemberLevelDao) Save(db *gorm.DB, model *entity.UmsMemberLevel) (*entity.UmsMemberLevel, error) {
	tx := db.Create(model)
	if tx.RowsAffected == 0 {
		return nil, errors.New("保存失败")
	}
	return model, nil
}
