package dao

import (
	"errors"
	"gorm.io/gorm"
	"member-service/common"
	"member-service/dao/entity"
)

var (
	UmsMemberDao = &umsMemberDao{}
)

type umsMemberDao struct {
}

func (this *umsMemberDao) FindById(db *gorm.DB, id int64) (*entity.UmsMember, error) {
	model := &entity.UmsMember{}
	db.Find(model, id)
	if model.Id == 0 {
		return nil, errors.New("无此用户")
	}
	return model, nil
}

func (this *umsMemberDao) FindAll(db *gorm.DB, query *common.PageQuery) ([]*entity.UmsMember, int64, error) {
	tx := db.Scopes(Page(int(query.Page), int(query.Size)))

	//把数据封装到any里
	var models []*entity.UmsMember

	//直接拼接所有条件
	model := query.Model.(*entity.UmsMember)

	//count不包含limit条件
	var count int64
	tx.Model(&model).Count(&count)

	err := tx.Where(model).Find(&models).Error
	if err != nil {
		return nil, 0, err
	}
	return models, count, nil
}

func (this *umsMemberDao) DeleteById(db *gorm.DB, id int64) error {
	tx := db.Delete(&entity.UmsMember{}, id)
	if tx.Error != nil {
		return errors.New("无效删除")
	}
	return nil
}
func (this *umsMemberDao) UpdateById(db *gorm.DB, id int64, model *entity.UmsMember) error {
	model.Id = id
	tx := db.Updates(model)
	if tx.Error != nil {
		return errors.New("无效更新")
	}
	return nil
}

func (this *umsMemberDao) Save(db *gorm.DB, model *entity.UmsMember) (*entity.UmsMember, error) {
	tx := db.Create(model)
	if tx.RowsAffected == 0 {
		return nil, errors.New("保存失败")
	}
	return model, nil
}
