package entity

import (
	"reflect"
)

type UmsMemberLevel struct {
	Id                    int64  `json:"id" gorm:"primaryKey" form:"id" `
	Name                  string `json:"name" form:"name"`
	GrowthPoint           int64  `json:"growth_point" form:"growth_point"`
	DefaultStatus         int64  `json:"default_status" form:"default_status"`
	FreeFreightPoint      string `json:"free_freight_point" form:"free_freight_point"`
	CommentGrowthPoint    int64  `json:"comment_growth_point" form:"comment_growth_point"`
	PriviledgeFreeFreight int64  `json:"priviledge_free_freight" form:"priviledge_free_freight"`
	PriviledgeMemberPrice int64  `json:"priviledge_member_price" form:"priviledge_member_price"`
	PriviledgeBirthday    int64  `json:"priviledge_birthday" form:"priviledge_birthday"`
	Note                  string `json:"note" form:"note"`
}

func (this *UmsMemberLevel) TableName() string {
	return "ums_member_level"
}

func (t *UmsMemberLevel) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &UmsMemberLevel{})
}
