package entity

import (
	"gorm.io/plugin/soft_delete"
	"reflect"
	"time"
)

type UmsMember struct {
	Id          int64                 `json:"id" gorm:"primaryKey" form:"id" `
	LevelId     int64                 `json:"level_id" form:"level_id"`
	Username    string                `json:"username" form:"username"`
	Password    string                `json:"password" form:"password"`
	Nickname    string                `json:"nickname" form:"nickname"`
	Mobile      string                `json:"mobile" form:"mobile"`
	Email       string                `json:"email" form:"email"`
	Header      string                `json:"header" form:"header"`
	Gender      int64                 `json:"gender" form:"gender"`
	Birth       time.Time             `json:"birth" form:"birth"`
	City        string                `json:"city" form:"city"`
	Job         string                `json:"job" form:"job"`
	Sign        string                `json:"sign" form:"sign"`
	SourceType  int64                 `json:"source_type" form:"source_type"`
	Integration int64                 `json:"integration" form:"integration"`
	Growth      int64                 `json:"growth" form:"growth"`
	Status      soft_delete.DeletedAt `json:"status" form:"status" gorm:"softDelete:flag"`
	CreateTime  time.Time             `json:"create_time" form:"create_time" gorm:"autoCreateTime"`
}

func (this *UmsMember) TableName() string {
	return "ums_member"
}

func (t *UmsMember) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &UmsMember{})
}
