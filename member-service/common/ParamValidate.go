package common

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

func IntParamValidate(context *gin.Context) (int64, error) {
	param := context.Param("id")
	id, err := strconv.ParseInt(param, 10, 64)
	return id, err
}
