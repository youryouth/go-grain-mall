package common

type PageQuery struct {
	Page  int         `json:"page"`
	Size  int         `json:"size"`
	Model interface{} `json:"model"`
}

type Page struct {
	Total int64       `json:"total"`
	Data  interface{} `json:"data"`
}
