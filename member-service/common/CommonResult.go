package common

var (
	OK    = ResponseCode(200, "ok")
	Error = ResponseCode(500, "other error")
)

type CommonReponse struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// WithMsg 自定义响应信息
func (t *CommonReponse) WithMsg(message string) *CommonReponse {
	return &CommonReponse{
		Code: t.Code,
		Msg:  message,
		Data: t.Data,
	}
}

// WithData 追加响应数据,这里又新创建了一个CommonReponse对象
func (t *CommonReponse) WithData(data interface{}) *CommonReponse {
	return &CommonReponse{
		Code: t.Code,
		Msg:  t.Msg,
		Data: data,
	}
}

func ResponseCode(code int, msg string) *CommonReponse {
	return &CommonReponse{
		Code: code,
		Msg:  msg,
		Data: nil,
	}
}
