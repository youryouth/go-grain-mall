package service

import (
	"member-service/common"
	"member-service/dao"
	"member-service/dao/entity"
)

var (
	UmsMemberService = &umsMemberService{}
)

type umsMemberService struct {
}

func (t *umsMemberService) DeleteById(id int64) *common.CommonReponse {
	_, err := dao.UmsMemberDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.UmsMemberDao.DeleteById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(nil)
}

func (t *umsMemberService) UpdateById(id int64, model *entity.UmsMember) *common.CommonReponse {
	_, err := dao.UmsMemberDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.UmsMemberDao.UpdateById(dao.DB, id, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK
}

func (t *umsMemberService) Save(model *entity.UmsMember) *common.CommonReponse {
	data, err := dao.UmsMemberDao.Save(dao.DB, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *umsMemberService) FindById(id int64) *common.CommonReponse {
	data, err := dao.UmsMemberDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *umsMemberService) FindAll(pageQuery *common.PageQuery) *common.CommonReponse {
	all, i, err := dao.UmsMemberDao.FindAll(dao.DB, pageQuery)
	page := &common.Page{
		Total: i,
		Data:  all,
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(page)
}
