package service

import (
	"member-service/common"
	"member-service/dao"
	"member-service/dao/entity"
)

var (
	UmsMemberLevelService = &umsMemberLevelService{}
)

type umsMemberLevelService struct {
}

func (t *umsMemberLevelService) DeleteById(id int64) *common.CommonReponse {
	_, err := dao.UmsMemberLevelDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.UmsMemberLevelDao.DeleteById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(nil)
}

func (t *umsMemberLevelService) UpdateById(id int64, model *entity.UmsMemberLevel) *common.CommonReponse {
	_, err := dao.UmsMemberLevelDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.UmsMemberLevelDao.UpdateById(dao.DB, id, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK
}

func (t *umsMemberLevelService) Save(model *entity.UmsMemberLevel) *common.CommonReponse {
	data, err := dao.UmsMemberLevelDao.Save(dao.DB, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *umsMemberLevelService) FindById(id int64) *common.CommonReponse {
	data, err := dao.UmsMemberLevelDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *umsMemberLevelService) FindAll(pageQuery *common.PageQuery) *common.CommonReponse {
	all, i, err := dao.UmsMemberLevelDao.FindAll(dao.DB, pageQuery)
	page := &common.Page{
		Total: i,
		Data:  all,
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(page)
}
