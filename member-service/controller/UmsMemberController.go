package controller

import (
	"github.com/gin-gonic/gin"
	"member-service/common"
	"member-service/dao/entity"
	"member-service/service"
	"member-service/utils/result"
	"net/http"
	"strconv"
)

type UmsMemberController struct {
}

func (t *UmsMemberController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/member")
	group.GET("/umsMember/findAll/:page/:size", t.FindAll)
	group.GET("/umsMember/:id", t.FindById)
	group.DELETE("/umsMember/:id", t.DeleteById)
	group.PUT("/umsMember/:id", t.UpdateById)
	group.POST("/umsMember", t.Save)
}

func (t *UmsMemberController) DeleteById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.UmsMemberService.DeleteById(id)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberController) UpdateById(context *gin.Context) {
	model := &entity.UmsMember{}
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	err = context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	response := service.UmsMemberService.UpdateById(id, model)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberController) Save(context *gin.Context) {
	model := &entity.UmsMember{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	response := service.UmsMemberService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberController) FindById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.UmsMemberService.FindById(id)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.UmsMember{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.UmsMemberService.FindAll(query)
	context.JSON(http.StatusOK, response)
}
