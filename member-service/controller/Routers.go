package controller

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

//在这里添加你的路由树
func Add() {
	umsMemberController := &UmsMemberController{}
	umsMemberLevelController := &UmsMemberLevelController{}
	Include(
		umsMemberController,
		umsMemberLevelController,
	)

}

func AddMiddleWare(engine *gin.Engine) {
	engine.Use(cors.Default())
}

// 添加路由
func Include(center ...CenterRouter) {
	for _, value := range center {
		options = append(options, value)
	}
}

type CenterRouter interface {
	RegisterRouter(engine *gin.Engine)
}

var options []CenterRouter

// Init 初始化
func Init(engine *gin.Engine) {

	//添加全局中间件
	AddMiddleWare(engine)

	//利用多态,给每个方法传入engine
	Add()
	for _, value := range options {
		value.RegisterRouter(engine)
	}
}
