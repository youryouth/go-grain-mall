package controller

import (
	"github.com/gin-gonic/gin"
	"member-service/common"
	"member-service/dao/entity"
	"member-service/service"
	"member-service/utils/result"
	"net/http"
	"strconv"
)

type UmsMemberLevelController struct {
}

func (t *UmsMemberLevelController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/member")
	group.GET("/umsMemberLevel/findAll/:page/:size", t.FindAll)
	group.GET("/umsMemberLevel/:id", t.FindById)
	group.DELETE("/umsMemberLevel/:id", t.DeleteById)
	group.PUT("/umsMemberLevel/:id", t.UpdateById)
	group.POST("/umsMemberLevel", t.Save)
}

func (t *UmsMemberLevelController) DeleteById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.UmsMemberLevelService.DeleteById(id)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberLevelController) UpdateById(context *gin.Context) {
	model := &entity.UmsMemberLevel{}
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	err = context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	response := service.UmsMemberLevelService.UpdateById(id, model)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberLevelController) Save(context *gin.Context) {
	model := &entity.UmsMemberLevel{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	response := service.UmsMemberLevelService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberLevelController) FindById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.UmsMemberLevelService.FindById(id)
	context.JSON(http.StatusOK, response)
}

func (t *UmsMemberLevelController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.UmsMemberLevel{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.UmsMemberLevelService.FindAll(query)
	context.JSON(http.StatusOK, response)
}
