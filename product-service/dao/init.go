package dao

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var dataBaseName = "guli_pms"
var (
	url = "root:root@tcp(127.0.0.1:3306)/" + dataBaseName + "?charset=utf8mb4&parseTime=True&loc=Local"
	//默认的日志打印
	config = &gorm.Config{Logger: logger.Default.LogMode(logger.Info)}
	DB, _  = gorm.Open(mysql.Open(url), config)
)

//初始化
func init() {
}

func Page(page int, pageSize int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if page <= 0 {
			page = 1
		}
		if pageSize <= 0 {

			pageSize = 10
		}
		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}
