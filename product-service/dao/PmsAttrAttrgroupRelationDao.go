package dao

import (
	"errors"
	"gorm.io/gorm"
	"product-service/common"
	"product-service/dao/entity"
	"product-service/utils/vo"
)

var (
	PmsAttrAttrgroupRelationDao = &pmsAttrAttrgroupRelationDao{}
)

type pmsAttrAttrgroupRelationDao struct {
}

func (this *pmsAttrAttrgroupRelationDao) FindByGroupId(db *gorm.DB, id int64) ([]*entity.PmsAttrAttrgroupRelation, error) {
	var models []*entity.PmsAttrAttrgroupRelation
	db.Where("attr_group_id=?", id).Find(&models)
	if len(models) == 0 {
		return nil, errors.New("无关联数据")
	}
	return models, nil
}
func (this *pmsAttrAttrgroupRelationDao) FindByGroupIdAndAttrId(db *gorm.DB, id int64, attrId int64) (*entity.PmsAttrAttrgroupRelation, error) {
	var model *entity.PmsAttrAttrgroupRelation
	db.Where("attr_group_id=?", id).Where("attr_id=?", attrId).Find(&model)
	if model.Id == 0 {
		return nil, errors.New("无关联数据")
	}
	return model, nil
}

func (this *pmsAttrAttrgroupRelationDao) FindById(db *gorm.DB, id int64) (*entity.PmsAttrAttrgroupRelation, error) {
	model := &entity.PmsAttrAttrgroupRelation{}
	db.Find(model, id)
	if model.Id == 0 {
		return nil, errors.New("无此用户")
	}
	return model, nil
}
func (this *pmsAttrAttrgroupRelationDao) FindArrGroupByAttrIdOne(db *gorm.DB, id int64) (*entity.PmsAttrAttrgroupRelation, error) {
	model := &entity.PmsAttrAttrgroupRelation{}
	//这里只查一条数据
	db.Where("attr_id=?", id).First(model)
	if model.Id == 0 {
		return nil, errors.New("无此用户")
	}
	return model, nil
}

func (this *pmsAttrAttrgroupRelationDao) FindAll(db *gorm.DB, query *common.PageQuery) ([]*entity.PmsAttrAttrgroupRelation, int64, error) {
	tx := db.Scopes(Page(int(query.Page), int(query.Size)))

	//把数据封装到any里
	var models []*entity.PmsAttrAttrgroupRelation

	//直接拼接所有条件
	model := query.Model.(*entity.PmsAttrAttrgroupRelation)

	//count不包含limit条件
	var count int64
	tx.Model(&model).Count(&count)

	err := tx.Where(model).Find(&models).Error
	if err != nil {
		return nil, 0, err
	}
	return models, count, nil
}

func (this *pmsAttrAttrgroupRelationDao) DeleteBatch(db *gorm.DB, ids []int64) error {
	begin := db.Begin()
	tx := db.Delete(&entity.PmsAttrAttrgroupRelation{}, ids)
	if tx.Error != nil {
		begin.Rollback()
		return errors.New("无效删除")
	}
	begin.Commit()
	return nil
}

func (this *pmsAttrAttrgroupRelationDao) UpdateById(db *gorm.DB, id int64, model *entity.PmsAttrAttrgroupRelation) error {
	model.Id = id
	tx := db.Updates(model)
	if tx.Error != nil {
		return errors.New("无效更新")
	}
	return nil
}

func (this *pmsAttrAttrgroupRelationDao) UpdateGroupIdByAttrId(db *gorm.DB, id int64, groupId int64) error {
	tx := db.Model(&entity.PmsAttrAttrgroupRelation{}).
		Where("attr_id=?", id).
		Update("attr_group_id", groupId)
	if tx.Error != nil {
		return errors.New("无效更新")
	}
	return nil
}

func (this *pmsAttrAttrgroupRelationDao) Save(db *gorm.DB, model *entity.PmsAttrAttrgroupRelation) (*entity.PmsAttrAttrgroupRelation, error) {
	tx := db.Create(model)
	if tx.RowsAffected == 0 {
		return nil, errors.New("保存失败")
	}
	return model, nil
}

func (this *pmsAttrAttrgroupRelationDao) DeleteById(db *gorm.DB, id int64, attrId int64) error {
	err := db.Where("attr_id=?", attrId).Where("attr_group_id", id).Delete(&entity.PmsAttrAttrgroupRelation{}).Error
	if err != nil {
		return err
	}
	return nil
}

func (this *pmsAttrAttrgroupRelationDao) UnLinkAttrByGroupId(db *gorm.DB, query *common.PageQuery, vo *vo.PmsAttrRelationVo) ([]*entity.PmsAttr, int64, error) {
	sql := `
	    SELECT  * FROM pms_attr ptr1 
		WHERE ptr1.attr_type=1 
		AND ptr1.catelog_id=? 
		AND ptr1.attr_id 
		NOT IN (
		   SELECT prelation.attr_id FROM pms_attr_attrgroup_relation prelation 
			 WHERE prelation.attr_group_id=?
		)
	 `
	//count不包含limit条件
	var count int64
	db.Model(&entity.PmsAttrAttrgroupRelation{}).Count(&count)

	newDB := db.Scopes(Page(int(query.Page), int(query.Size)))
	rows, err := newDB.Raw(sql, vo.CatelogId, vo.AttrGroupId).Rows()
	if err != nil {
		return nil, 0, err
	}
	var models []*entity.PmsAttr
	for rows.Next() {
		err = db.ScanRows(rows, &models)
		if err != nil {
			return nil, 0, err
		}
	}
	return models, count, nil
}

func (this *pmsAttrAttrgroupRelationDao) GetAttrIdsByGroupId(db *gorm.DB, id int64) ([]int64, error) {
	var ids []int64
	err := db.Model(&entity.PmsAttrAttrgroupRelation{}).Select("attr_id").Where("attr_group_id=?", id).Find(&ids).Error
	if err != nil {
		return nil, err
	}
	return ids, nil
}
