package entity

import (
	"gorm.io/gorm"
	"reflect"
)

type PmsCategory struct {
	CatId        int64          `json:"cat_id"  form:"cat_id" gorm:"primaryKey" binding:"isdefault,number"`
	Name         string         `json:"name" form:"name" binding:"omitempty,gte=3"`
	ParentCid    int64          `json:"parent_cid" form:"parent_cid"`
	CatLevel     int64          `json:"cat_level" form:"cat_level"`
	ShowStatus   int            `json:"show_status" form:"show_status"  gorm:"default:1" binding:"oneof=0 1"`
	Sort         int64          `json:"sort" form:"sort"`
	Icon         string         `json:"icon" form:"icon"`
	ProductUnit  string         `json:"product_unit" form:"product_unit"`
	ProductCount int64          `json:"product_count" form:"product_count"`
	Children     []*PmsCategory `json:"children" form:"children" gorm:"-" `
	DeletedAt    gorm.DeletedAt `json:"deleted_at" form:"deleted_at"`
}

func (t *PmsCategory) TableName() string {
	return "pms_category"
}

func (t *PmsCategory) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &PmsCategory{})
}
