package entity

import (
	"reflect"
)

type PmsAttr struct {
	AttrId      int64  `json:"attr_id" gorm:"primaryKey" form:"attr_id" binding:"isdefault,number"`
	AttrName    string `json:"attr_name" form:"attr_name"`
	SearchType  int64  `json:"search_type" form:"search_type"`
	Icon        string `json:"icon" form:"icon"`
	ValueSelect string `json:"value_select" form:"value_select"`
	AttrType    int64  `json:"attr_type" form:"attr_type" binding:"oneof=0 1 2"`
	Enable      int64  `json:"enable" form:"enable"`
	CatelogId   int64  `json:"catelog_id" form:"catelog_id" binding:"required,number"`
	ShowDesc    int64  `json:"show_desc" form:"show_desc"`
	//额外字段
	GroupId int64 `json:"group_id" form:"group_id" gorm:"-"`
}

func (this *PmsAttr) TableName() string {
	return "pms_attr"
}

func (t *PmsAttr) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &PmsAttr{})
}
