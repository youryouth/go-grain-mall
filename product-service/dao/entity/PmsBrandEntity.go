package entity

import (
	"gorm.io/gorm"
	"reflect"
)

type PmsBrand struct {
	BrandId     int64          `json:"brand_id" gorm:"primaryKey" form:"brand_id" `
	Name        string         `json:"name" form:"name"`
	Logo        string         `json:"logo" form:"logo"`
	Descript    string         `json:"descript" form:"descript"`
	ShowStatus  int64          `json:"show_status" form:"show_status"`
	FirstLetter string         `json:"first_letter" form:"first_letter"`
	Sort        int64          `json:"sort" form:"sort"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at" form:"deleted_at"`
}

func (this *PmsBrand) TableName() string {
	return "pms_brand"
}

func (t *PmsBrand) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &PmsBrand{})
}
