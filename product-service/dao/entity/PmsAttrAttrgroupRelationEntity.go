package entity

import (
	"reflect"
)

type PmsAttrAttrgroupRelation struct {
	Id          int64 `json:"id" gorm:"primaryKey" form:"id" `
	AttrId      int64 `json:"attr_id" form:"attr_id"`
	AttrGroupId int64 `json:"attr_group_id" form:"attr_group_id"`
	AttrSort    int64 `json:"attr_sort" form:"attr_sort"`
}

func (this *PmsAttrAttrgroupRelation) TableName() string {
	return "pms_attr_attrgroup_relation"
}

func (t *PmsAttrAttrgroupRelation) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &PmsAttrAttrgroupRelation{})
}
