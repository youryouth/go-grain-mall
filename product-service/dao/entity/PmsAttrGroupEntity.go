package entity

import "reflect"

type PmsAttrGroup struct {
	AttrGroupId   int64  `json:"attr_group_id" gorm:"primaryKey" form:"attr_group_id" binding:"isdefault,number"`
	AttrGroupName string `json:"attr_group_name" form:"attr_group_name"`
	Sort          int64  `json:"sort" form:"sort"`
	Descript      string `json:"descript" form:"descript"`
	Icon          string `json:"icon" form:"icon"`
	CatelogId     int64  `json:"catelog_id" form:"catelog_id"`
}

func (this *PmsAttrGroup) TableName() string {
	return "pms_attr_group"
}

func (t *PmsAttrGroup) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &PmsAttrGroup{})
}
