package entity

import "reflect"

type PmsCategoryBrandRelation struct {
	Id          int64  `json:"id" gorm:"primaryKey" form:"id" `
	BrandId     int64  `json:"brand_id" form:"brand_id"`
	CatelogId   int64  `json:"catelog_id" form:"catelog_id"`
	BrandName   string `json:"brand_name" form:"brand_name"`
	CatelogName string `json:"catelog_name" form:"catelog_name"`
}

func (this *PmsCategoryBrandRelation) TableName() string {
	return "pms_category_brand_relation"
}

func (t *PmsCategoryBrandRelation) IsStructEmpty() bool {
	return reflect.DeepEqual(t, &PmsCategoryBrandRelation{})
}
