package dao

import (
	"errors"
	"gorm.io/gorm"
	"product-service/common"
	"product-service/dao/entity"
)

var (
	PmsBrandDao = &pmsBrandDao{}
)

type pmsBrandDao struct {
}

func (this *pmsBrandDao) FindById(db *gorm.DB, id int64) (*entity.PmsBrand, error) {
	model := &entity.PmsBrand{}
	db.Find(model, id)
	if model.BrandId == 0 {
		return nil, errors.New("无此用户")
	}
	return model, nil
}

func (this *pmsBrandDao) FindAll(db *gorm.DB, query *common.PageQuery) ([]*entity.PmsBrand, int64, error) {
	tx := db.Scopes(Page(int(query.Page), int(query.Size)))

	//把数据封装到any里
	var models []*entity.PmsBrand

	//直接拼接所有条件
	model := query.Model.(*entity.PmsBrand)

	//count不包含limit条件
	var count int64
	tx.Model(&model).Count(&count)

	err := tx.Where(model).Find(&models).Error
	if err != nil {
		return nil, 0, err
	}
	return models, count, nil
}

func (this *pmsBrandDao) DeleteById(db *gorm.DB, id int64) error {
	tx := db.Delete(&entity.PmsBrand{}, id)
	if tx.Error != nil {
		return errors.New("无效删除")
	}
	return nil
}
func (this *pmsBrandDao) UpdateById(db *gorm.DB, id int64, model *entity.PmsBrand) error {
	model.BrandId = id
	tx := db.Updates(model)
	if tx.Error != nil {
		return errors.New("无效更新")
	}
	return nil
}

func (this *pmsBrandDao) Save(db *gorm.DB, model *entity.PmsBrand) (*entity.PmsBrand, error) {
	tx := db.Create(model)
	if tx.RowsAffected == 0 {
		return nil, errors.New("保存失败")
	}
	return model, nil
}
