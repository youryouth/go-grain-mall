package main

import (
	"github.com/gin-gonic/gin"
	"github.com/go-kratos/etcd/registry"
	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/transport/http"
	_ "github.com/go-playground/validator/v10"
	etcd "go.etcd.io/etcd/client/v3"
	"log"
	"product-service/controller"
)

var (
	Name    = "product-service"
	Version = "v1.0.0"
	httpSrv *http.Server
)

func main() {
	//使用etcd client创建Client,此时可以直接操作etcd了
	client, err := etcd.New(etcd.Config{
		Endpoints: []string{"127.0.0.1:2379"},
	})
	if err != nil {
		log.Fatal(err)
	}
	//把client封装成kratos里的register
	reg := registry.New(client)

	//注册路由
	router := gin.Default()
	controller.Init(router)

	//封装gin到kratos里
	httpSrv = http.NewServer(
		http.Address(":8000"),
	)
	httpSrv.HandlePrefix("/", router)
	app := kratos.New(
		kratos.Name(Name),
		kratos.Server(
			httpSrv,
		),
		//加入register,此时服务的信息会被kratos放到etcd里
		kratos.Registrar(reg),
	)
	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
