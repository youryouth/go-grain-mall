package service

import (
	"product-service/common"
	"product-service/dao"
	"product-service/dao/entity"
)

var (
	PmsCategoryBrandRelationService = &pmsCategoryBrandRelationService{}
)

type pmsCategoryBrandRelationService struct {
}

func (t *pmsCategoryBrandRelationService) Save(model *entity.PmsCategoryBrandRelation) *common.CommonReponse {
	//查找分类和品牌的名称
	brandModel, err := dao.PmsBrandDao.FindById(dao.DB, model.BrandId)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}

	categoryModel, err := dao.PmsCategoryDao.FindById(dao.DB, model.CatelogId)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	model.BrandName = brandModel.Name
	model.CatelogName = categoryModel.Name
	data, err := dao.PmsCategoryBrandRelationDao.Save(dao.DB, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *pmsCategoryBrandRelationService) FindAll(pageQuery *common.PageQuery) *common.CommonReponse {
	all, i, err := dao.PmsCategoryBrandRelationDao.FindAll(dao.DB, pageQuery)
	page := &common.Page{
		Total: i,
		Data:  all,
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(page)
}

func (t *pmsCategoryBrandRelationService) FindByBrandId(id int64) *common.CommonReponse {
	data, err := dao.PmsCategoryBrandRelationDao.FindByBrandId(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *pmsCategoryBrandRelationService) FindBrandsBycateId(id int64) *common.CommonReponse {
	data, err := dao.PmsCategoryBrandRelationDao.FindBrandsBycateId(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}
