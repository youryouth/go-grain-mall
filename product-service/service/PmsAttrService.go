package service

import (
	"product-service/common"
	"product-service/dao"
	"product-service/dao/entity"
	"product-service/utils"
	"product-service/utils/vo"
)

var (
	PmsAttrService = &pmsAttrService{}
)

type pmsAttrService struct {
}

func (t *pmsAttrService) DeleteById(id int64) *common.CommonReponse {
	_, err := dao.PmsAttrDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.PmsAttrDao.DeleteById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(nil)
}

func (t *pmsAttrService) UpdateById(id int64, model *entity.PmsAttr) *common.CommonReponse {

	_, err := dao.PmsAttrDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}

	begin := dao.DB.Begin()
	err = dao.PmsAttrDao.UpdateById(dao.DB, id, model)
	if err != nil {
		begin.Rollback()
		return common.Error.WithMsg(err.Error())
	}
	//判断type如果是AttrBase才需要修改中间表
	if model.AttrType == utils.AttrBase && model.GroupId != 0 {
		//修改中间表,判断是否传入了,传入了再修改
		if model.GroupId != 0 {
			err = dao.PmsAttrAttrgroupRelationDao.UpdateGroupIdByAttrId(begin, model.AttrId, model.GroupId)
			if err != nil {
				begin.Rollback()
				return common.Error.WithMsg(err.Error())
			}
		}
	}

	begin.Commit()
	return common.OK
}

func (t *pmsAttrService) Save(model *entity.PmsAttr) *common.CommonReponse {
	//开启事务
	begin := dao.DB.Begin()
	data, err := dao.PmsAttrDao.Save(begin, model)
	if err != nil {
		begin.Rollback()
		return common.Error.WithMsg(err.Error())
	}

	//判断type如果是AttrBase才需要保存到中间表
	if model.AttrType == utils.AttrBase && model.GroupId != 0 {
		//保存到中间表
		relation := &entity.PmsAttrAttrgroupRelation{
			AttrId:      data.AttrId,
			AttrGroupId: data.GroupId,
		}
		_, err = dao.PmsAttrAttrgroupRelationDao.Save(begin, relation)
		if err != nil {
			begin.Rollback()
			return common.Error.WithMsg(err.Error())
		}
	}
	begin.Commit()
	return common.OK.WithData(data)
}

func (t *pmsAttrService) FindById(id int64) *common.CommonReponse {
	//原始数据
	data, err := dao.PmsAttrDao.FindById(dao.DB, id)
	//拼接分类id和分组id
	array, err := dao.PmsCategoryDao.FindCategoryArray(dao.DB, data.CatelogId)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}

	group := &entity.PmsAttrAttrgroupRelation{}
	//判断type如果是AttrBase才需要从中间表查找分组信息
	if data.AttrType == utils.AttrBase {
		group, err = dao.PmsAttrAttrgroupRelationDao.FindArrGroupByAttrIdOne(dao.DB, data.AttrId)
		if err != nil {
			return common.Error.WithMsg(err.Error())
		}
	}
	updateVo := vo.PmsAttrUpdateVo{
		PmsAttr:        data,
		CatelogIdArray: array,
		AttrGroupId:    group.AttrGroupId,
	}
	return common.OK.WithData(updateVo)
}

func (t *pmsAttrService) FindAll(pageQuery *common.PageQuery) *common.CommonReponse {
	//原始数据
	all, i, err := dao.PmsAttrDao.FindAll(dao.DB, pageQuery)
	//这里直接进行了初始化,防止all为nil,然后返回给前台一个null,应该返回给前台空数组
	vos := []*vo.PmsAttrVo{}
	for index, _ := range all {
		value := &vo.PmsAttrVo{}
		value.PmsAttr = all[index]

		//当AttrType为base的时候才去查询分组数据,如果是销售属性,则没有分组信息
		if value.AttrType == utils.AttrBase {
			//根据属性id查找中间表找出分组id
			one, err := dao.PmsAttrAttrgroupRelationDao.FindArrGroupByAttrIdOne(dao.DB, value.AttrId)
			//当中间表有,则再去查找
			if err == nil {
				attrGroup, _ := dao.PmsAttrGroupDao.FindById(dao.DB, one.AttrGroupId)
				value.AttrGroupName = attrGroup.AttrGroupName
			}
		}

		//根据分类id查出分类名称
		cate, _ := dao.PmsCategoryDao.FindById(dao.DB, value.CatelogId)
		value.AttrCateLogName = cate.Name
		vos = append(vos, value)
	}
	page := &common.Page{
		Total: i,
		Data:  vos,
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(page)
}
