package service

import (
	"product-service/common"
	"product-service/dao"
	"product-service/dao/entity"
	"product-service/utils/result"
	"product-service/utils/vo"
)

var (
	PmsAttrAttrgroupRelationService = &pmsAttrAttrgroupRelationService{}
)

type pmsAttrAttrgroupRelationService struct {
}

func (t *pmsAttrAttrgroupRelationService) FindByGroupId(id int64) *common.CommonReponse {
	data, err := dao.PmsAttrAttrgroupRelationDao.FindByGroupId(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	var attrs []*entity.PmsAttr
	//查询关联的信息
	for _, value := range data {
		model, _ := dao.PmsAttrDao.FindById(dao.DB, value.AttrId)
		attrs = append(attrs, model)
	}
	return common.OK.WithData(attrs)
}

func (t *pmsAttrAttrgroupRelationService) DeleteBatch(id int64) *common.CommonReponse {
	//先根据group查询出相关的数据
	groups, _ := dao.PmsAttrAttrgroupRelationDao.FindByGroupId(dao.DB, id)
	var ids []int64
	for _, model := range groups {
		ids = append(ids, model.Id)
	}
	//批量删除
	err := dao.PmsAttrAttrgroupRelationDao.DeleteBatch(dao.DB, ids)
	if err != nil {
		return result.AttrRelationDeleteBatchError
	}

	return common.OK.WithData(nil)
}

func (t *pmsAttrAttrgroupRelationService) UpdateById(id int64, model *entity.PmsAttrAttrgroupRelation) *common.CommonReponse {
	_, err := dao.PmsAttrAttrgroupRelationDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.PmsAttrAttrgroupRelationDao.UpdateById(dao.DB, id, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK
}

func (t *pmsAttrAttrgroupRelationService) Save(model *entity.PmsAttrAttrgroupRelation) *common.CommonReponse {
	//先查询attr_id和group_id存不存在
	_, err := dao.PmsAttrDao.FindById(dao.DB, model.AttrId)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	_, err = dao.PmsAttrGroupDao.FindById(dao.DB, model.AttrId)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	data, err := dao.PmsAttrAttrgroupRelationDao.Save(dao.DB, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *pmsAttrAttrgroupRelationService) FindById(id int64) *common.CommonReponse {
	data, err := dao.PmsAttrAttrgroupRelationDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *pmsAttrAttrgroupRelationService) FindAll(pageQuery *common.PageQuery) *common.CommonReponse {
	all, i, err := dao.PmsAttrAttrgroupRelationDao.FindAll(dao.DB, pageQuery)
	page := &common.Page{
		Total: i,
		Data:  all,
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(page)
}

func (t *pmsAttrAttrgroupRelationService) DeleteById(id int64, attrId int64) *common.CommonReponse {
	//先根据group查询出相关的数据
	model, err := dao.PmsAttrAttrgroupRelationDao.FindByGroupIdAndAttrId(dao.DB, id, attrId)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.PmsAttrAttrgroupRelationDao.DeleteById(dao.DB, model.AttrGroupId, model.AttrId)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(nil)
}

func (t *pmsAttrAttrgroupRelationService) UnLinkAttrByGroupId(pageQuery *common.PageQuery, vo *vo.PmsAttrRelationVo) *common.CommonReponse {
	all, i, err := dao.PmsAttrAttrgroupRelationDao.UnLinkAttrByGroupId(dao.DB, pageQuery, vo)
	page := &common.Page{
		Total: i,
		Data:  all,
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(page)
}

func (t *pmsAttrAttrgroupRelationService) GetAttrGroupAndAttrsById(id int64) *common.CommonReponse {
	//通过分类id查找所有的attr_group
	attrGroups, err := dao.PmsAttrGroupDao.FindAttrGroupsByCateId(dao.DB, id)
	var GroupVos []*vo.PmsAttrGroupVo
	for _, group := range attrGroups {
		vo := &vo.PmsAttrGroupVo{}
		vo.PmsAttrGroup = group
		attrIds, _ := dao.PmsAttrAttrgroupRelationDao.GetAttrIdsByGroupId(dao.DB, group.AttrGroupId)
		for _, attr_id := range attrIds {
			attrModel, _ := dao.PmsAttrDao.FindById(dao.DB, attr_id)
			vo.Attrs = append(vo.Attrs, attrModel)
		}
		GroupVos = append(GroupVos, vo)
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(GroupVos)
}
