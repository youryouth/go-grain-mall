package service

import (
	"product-service/common"
	"product-service/dao"
	"product-service/dao/entity"
)

var (
	PmsAttrGroupService = &pmsAttrGroupService{}
)

type pmsAttrGroupService struct {
}

func (t *pmsAttrGroupService) DeleteById(id int64) *common.CommonReponse {
	_, err := dao.PmsAttrGroupDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.PmsAttrGroupDao.DeleteById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(nil)
}

func (t *pmsAttrGroupService) UpdateById(id int64, model *entity.PmsAttrGroup) *common.CommonReponse {
	_, err := dao.PmsAttrGroupDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	err = dao.PmsAttrGroupDao.UpdateById(dao.DB, id, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK
}

func (t *pmsAttrGroupService) Save(model *entity.PmsAttrGroup) *common.CommonReponse {
	data, err := dao.PmsAttrGroupDao.Save(dao.DB, model)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(data)
}

func (t *pmsAttrGroupService) FindById(id int64) *common.CommonReponse {
	model, err := dao.PmsAttrGroupDao.FindById(dao.DB, id)
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(model)
}

func (t *pmsAttrGroupService) FindAll(pageQuery *common.PageQuery) *common.CommonReponse {
	all, i, err := dao.PmsAttrGroupDao.FindAll(dao.DB, pageQuery)
	page := &common.Page{
		Total: i,
		Data:  all,
	}
	if err != nil {
		return common.Error.WithMsg(err.Error())
	}
	return common.OK.WithData(page)
}
