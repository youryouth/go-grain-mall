package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"product-service/common"
	"product-service/dao/entity"
	"product-service/service"
	"product-service/utils/result"
	"strconv"
)

type PmsAttrController struct {
}

func (t *PmsAttrController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/product")
	group.GET("/pmsAttr/findAll/:page/:size", t.FindAll)
	group.GET("/pmsAttr/:id", t.FindById)
	group.DELETE("/pmsAttr/:id", t.DeleteById)
	group.PUT("/pmsAttr/:id", t.UpdateById)
	group.POST("/pmsAttr", t.Save)
}

func (t *PmsAttrController) DeleteById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrService.DeleteById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrController) UpdateById(context *gin.Context) {
	model := &entity.PmsAttr{}
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	err = context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	response := service.PmsAttrService.UpdateById(id, model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrController) Save(context *gin.Context) {
	model := &entity.PmsAttr{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	//判断groupId
	//if model.GroupId == 0 {
	//	context.JSON(http.StatusOK, result.AttrSaveGroupIdWithNone)
	//	return
	//}
	response := service.PmsAttrService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrController) FindById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrService.FindById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.PmsAttr{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.PmsAttrService.FindAll(query)
	context.JSON(http.StatusOK, response)
}
