package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"product-service/common"
	"product-service/dao/entity"
	"product-service/service"
	"product-service/utils/result"
	"strconv"
)

type PmsCategoryController struct {
}

func (t *PmsCategoryController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/product")
	group.GET("/pmsCategory/findAll/:page/:size", t.FindAll)
	group.GET("/pmsCategory/:id", t.FindById)
	group.GET("/pmsCategory/findTree", t.FindTree)
	group.DELETE("/pmsCategory/:id", t.DeleteById)
	group.PUT("/pmsCategory/:id", t.UpdateById)
	group.POST("/pmsCategory", t.Save)
	group.PUT("/pmsCategory/updateBatch", t.UpdateBatch)
	group.DELETE("/pmsCategory/deleteBatch", t.DeleteBatch)
	group.GET("/pmsCategory/findCatIdArray/:id", t.FindCatIdArray)
}

func (t *PmsCategoryController) FindCatIdArray(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsCategoryService.FindCatIdArray(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) FindTree(context *gin.Context) {
	response := service.PmsCategoryService.FindTree()
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) DeleteById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsCategoryService.DeleteById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) UpdateById(context *gin.Context) {
	model := &entity.PmsCategory{}
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	err = context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	response := service.PmsCategoryService.UpdateById(id, model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) Save(context *gin.Context) {
	model := &entity.PmsCategory{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	response := service.PmsCategoryService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) FindById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsCategoryService.FindById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.PmsCategory{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.PmsCategoryService.FindAll(query)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) UpdateBatch(context *gin.Context) {
	var models []*entity.PmsCategory
	err := context.ShouldBindJSON(&models)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能一个都不保存
	if len(models) == 0 {
		result.NilError(context)
		return
	}

	response := service.PmsCategoryService.UpdateBatch(models)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryController) DeleteBatch(context *gin.Context) {
	var ids []int64
	err := context.ShouldBindJSON(&ids)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能一个都不删除
	if len(ids) == 0 {
		result.NilError(context)
		return
	}
	response := service.PmsCategoryService.DeleteBatch(ids)
	context.JSON(http.StatusOK, response)
}
