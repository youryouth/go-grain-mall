package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"product-service/common"
	"product-service/dao/entity"
	"product-service/service"
	"product-service/utils/result"
	"strconv"
)

type PmsBrandController struct {
}

func (t *PmsBrandController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/product")
	group.GET("/pmsBrand/findAll/:page/:size", t.FindAll)
	group.GET("/pmsBrand/:id", t.FindById)
	group.DELETE("/pmsBrand/:id", t.DeleteById)
	group.PUT("/pmsBrand/:id", t.UpdateById)
	group.POST("/pmsBrand", t.Save)
}

func (t *PmsBrandController) DeleteById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsBrandService.DeleteById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsBrandController) UpdateById(context *gin.Context) {
	model := &entity.PmsBrand{}
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	err = context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	response := service.PmsBrandService.UpdateById(id, model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsBrandController) Save(context *gin.Context) {
	model := &entity.PmsBrand{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	response := service.PmsBrandService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsBrandController) FindById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsBrandService.FindById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsBrandController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.PmsBrand{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.PmsBrandService.FindAll(query)
	context.JSON(http.StatusOK, response)
}
