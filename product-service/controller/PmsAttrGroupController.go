package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"product-service/common"
	"product-service/dao/entity"
	"product-service/service"
	"product-service/utils/result"
	"strconv"
)

type PmsAttrGroupController struct {
}

func (t *PmsAttrGroupController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/product")
	group.GET("/pmsAttrGroup/findAll/:page/:size", t.FindAll)
	group.GET("/pmsAttrGroup/:id", t.FindById)
	group.DELETE("/pmsAttrGroup/:id", t.DeleteById)
	group.PUT("/pmsAttrGroup/:id", t.UpdateById)
	group.POST("/pmsAttrGroup", t.Save)
}

func (t *PmsAttrGroupController) DeleteById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrGroupService.DeleteById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrGroupController) UpdateById(context *gin.Context) {
	model := &entity.PmsAttrGroup{}
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	err = context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	response := service.PmsAttrGroupService.UpdateById(id, model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrGroupController) Save(context *gin.Context) {
	model := &entity.PmsAttrGroup{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	response := service.PmsAttrGroupService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrGroupController) FindById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrGroupService.FindById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrGroupController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.PmsAttrGroup{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.PmsAttrGroupService.FindAll(query)
	context.JSON(http.StatusOK, response)
}
