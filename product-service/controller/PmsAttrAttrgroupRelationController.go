package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"product-service/common"
	"product-service/dao/entity"
	"product-service/service"
	"product-service/utils/result"
	"product-service/utils/vo"
	"strconv"
)

type PmsAttrAttrgroupRelationController struct {
}

func (t *PmsAttrAttrgroupRelationController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/product")
	group.GET("/pmsAttrAttrgroupRelation/findAll/:page/:size", t.FindAll)
	group.GET("/pmsAttrAttrgroupRelation/:id", t.FindById)
	group.DELETE("/pmsAttrAttrgroupRelation/deleteBatchByGroupId/:id", t.DeleteBatch)
	group.DELETE("/pmsAttrAttrgroupRelation/:id/:attrId", t.DeleteById)
	group.PUT("/pmsAttrAttrgroupRelation/:id", t.UpdateById)
	group.POST("/pmsAttrAttrgroupRelation", t.Save)
	group.GET("/pmsAttrAttrgroupRelation/findByGroupId/:id", t.findByGroupId)
	group.GET("/pmsAttrAttrgroupRelation/unLinkAttrByGroupId/:page/:size", t.unLinkAttrByGroupId)
	group.GET("/pmsAttrAttrgroupRelation/getAttrGroupAndAttrsById/:id", t.getAttrGroupAndAttrsById)
}

func (t *PmsAttrAttrgroupRelationController) getAttrGroupAndAttrsById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrAttrgroupRelationService.GetAttrGroupAndAttrsById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) findByGroupId(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrAttrgroupRelationService.FindByGroupId(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) DeleteBatch(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrAttrgroupRelationService.DeleteBatch(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) DeleteById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	param := context.Param("attrId")
	attrId, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		result.IdError(context)
		return
	}

	response := service.PmsAttrAttrgroupRelationService.DeleteById(id, attrId)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) Save(context *gin.Context) {
	model := &entity.PmsAttrAttrgroupRelation{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	response := service.PmsAttrAttrgroupRelationService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) UpdateById(context *gin.Context) {
	model := &entity.PmsAttrAttrgroupRelation{}
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	err = context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	response := service.PmsAttrAttrgroupRelationService.UpdateById(id, model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) FindById(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsAttrAttrgroupRelationService.FindById(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.PmsAttrAttrgroupRelation{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.PmsAttrAttrgroupRelationService.FindAll(query)
	context.JSON(http.StatusOK, response)
}

func (t *PmsAttrAttrgroupRelationController) unLinkAttrByGroupId(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &vo.PmsAttrRelationVo{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.PmsAttrAttrgroupRelationService.UnLinkAttrByGroupId(query, model)
	context.JSON(http.StatusOK, response)
}
