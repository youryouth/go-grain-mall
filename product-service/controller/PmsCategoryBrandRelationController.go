package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"product-service/common"
	"product-service/dao/entity"
	"product-service/service"
	"product-service/utils/result"
	"strconv"
)

type PmsCategoryBrandRelationController struct {
}

func (t *PmsCategoryBrandRelationController) RegisterRouter(engine *gin.Engine) {
	group := engine.Group("/product")
	group.POST("/pmsCategoryBrandRelation", t.Save)
	//根据品牌id查找所有分类
	group.GET("/pmsCategoryBrandRelation/findByBrandId/:id", t.FindByBrandId)
	//根据分类id查找所有品牌
	group.GET("/pmsCategoryBrandRelation/findBrandsBycateId/:id", t.findBrandsBycateId)

}

func (t *PmsCategoryBrandRelationController) FindByBrandId(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsCategoryBrandRelationService.FindByBrandId(id)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryBrandRelationController) Save(context *gin.Context) {
	model := &entity.PmsCategoryBrandRelation{}
	err := context.ShouldBind(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不能保存空结构
	if model.IsStructEmpty() {
		result.NilError(context)
		return
	}
	response := service.PmsCategoryBrandRelationService.Save(model)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryBrandRelationController) FindAll(context *gin.Context) {
	//直接忽略错误,因为page已经处理了这2个值为0的情况
	page, _ := strconv.Atoi(context.Param("page"))
	size, _ := strconv.Atoi(context.Param("size"))

	model := &entity.PmsCategoryBrandRelation{}
	err := context.ShouldBindQuery(model)
	if err != nil {
		result.OkError(context, err)
		return
	}
	//不管有没有查询的参数,都给它对象
	query := &common.PageQuery{
		Page:  page,
		Size:  size,
		Model: model,
	}
	response := service.PmsCategoryBrandRelationService.FindAll(query)
	context.JSON(http.StatusOK, response)
}

func (t *PmsCategoryBrandRelationController) findBrandsBycateId(context *gin.Context) {
	id, err := common.IntParamValidate(context)
	if err != nil {
		result.IdError(context)
		return
	}
	response := service.PmsCategoryBrandRelationService.FindBrandsBycateId(id)
	context.JSON(http.StatusOK, response)
}
