package vo

import "product-service/dao/entity"

type PmsAttrVo struct {
	*entity.PmsAttr
	AttrGroupName   string `json:"attr_group_name" form:"attr_group_name"`
	AttrCateLogName string `json:"attr_cate_log_name"  form:"attr_cate_log_name"`
}

type PmsAttrUpdateVo struct {
	*entity.PmsAttr
	CatelogIdArray []int64 `json:"catelog_id_array" form:"catelog_id_array"`
	AttrGroupId    int64   `json:"attr_group_id" form:"attr_group_id"`
}

type PmsAttrRelationVo struct {
	AttrGroupId int64 `json:"attr_group_id" form:"attr_group_id" binding:"required"`
	CatelogId   int64 `json:"catelog_id" form:"catelog_id" binding:"required"`
}
