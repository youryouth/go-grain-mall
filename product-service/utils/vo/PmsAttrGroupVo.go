package vo

import "product-service/dao/entity"

type PmsAttrGroupVo struct {
	*entity.PmsAttrGroup
	Attrs []*entity.PmsAttr
}
