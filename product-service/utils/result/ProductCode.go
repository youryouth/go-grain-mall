package result

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"product-service/common"
)

var (
	CategoryUpdateBatchError = common.ResponseCode(10001, "Category批量保存失败")
	CategoryDeleteBatchError = common.ResponseCode(10002, "Category批量删除失败")
	//AttrSaveGroupIdWithNone      = common.ResponseCode(10003, "Attr没有传递GroupId用于保存")
	AttrRelationDeleteBatchError = common.ResponseCode(10004, "AttrRelation批量删除失败")
)

func OkError(context *gin.Context, err error) {
	context.JSON(http.StatusOK, common.Error.WithMsg(err.Error()))
}
func IdError(context *gin.Context) {
	context.JSON(http.StatusOK, common.Error.WithMsg("id错误"))
}

func NilError(context *gin.Context) {
	context.JSON(http.StatusOK, common.Error.WithMsg("不能保存空数据"))
}
