module product-service

go 1.16

require (
	github.com/aliyun/aliyun-oss-go-sdk v2.1.10+incompatible
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.4
	github.com/go-kratos/etcd v0.1.1
	github.com/go-kratos/kratos/v2 v2.0.0-rc6
	github.com/go-playground/validator/v10 v10.4.1
	github.com/satori/go.uuid v1.2.0 // indirect
	go.etcd.io/etcd/client/v3 v3.5.0
	golang.org/x/time v0.0.0-20210723032227-1f47c861a9ac // indirect
	google.golang.org/genproto v0.0.0-20210629200056-84d6f6074151 // indirect
	google.golang.org/grpc v1.39.0 // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.11
)
